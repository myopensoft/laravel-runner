# Laravel Runner

To generate running number that managed by database as a storage. For Laravel 5.4+, 6.0+

## Installation

```
composer require myopensoft/runner
```

```
php artisan migrate
```

```PHP
use MyOpensoft\Runner\Runner;
```

## Generate number
1. There will be 5 indexed rule (`{rule_1}` ... `{rule_5}`) and 2 non-indexed rule (`{n_rule_1}` and `{n_rule_2}`).
2. `{raw}` will get raw number.
3. `{p_raw,<number>}` will generate left padding from raw number.

```PHP
// generate TTFN-WPPJ-(P)-000001-2020
Runner::generate("{rule_1}-{rule_2}-{n_rule_1}-{p_raw,6}-{rule_3}", "TTFM", "WPPJ", 2020, null, null, '(P)');
```

```PHP
// generate T20022200001 | T 20 02 22 00001
Runner::generate("{rule_1}{rule_2}{rule_3}{rule_4}{p_raw,5}", "T", '20', '02', '22');
```

## TODO
1. Test script

