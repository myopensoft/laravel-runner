<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompositIndexToRunners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->string('rule_1', 100)->change();
            $table->string('rule_2', 100)->change();
            $table->string('rule_3', 100)->change();
            $table->string('rule_4', 100)->change();
            $table->string('rule_5', 100)->change();
            $table->index(['rule_1', 'rule_2', 'rule_3', 'rule_4', 'rule_5'], 'runner_rules_index');
        });
    }
}
