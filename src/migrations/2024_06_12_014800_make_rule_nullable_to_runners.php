<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeRuleNullableToRunners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->string('rule_1', 100)->nullable()->change();
            $table->string('rule_2', 100)->nullable()->change();
            $table->string('rule_3', 100)->nullable()->change();
            $table->string('rule_4', 100)->nullable()->change();
            $table->string('rule_5', 100)->nullable()->change();
        });
    }
}
