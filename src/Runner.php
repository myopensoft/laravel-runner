<?php

namespace MyOpensoft\Runner;

class Runner
{
    /**
     * Generate running number by rule
     * @param string $format
     * @param string $rule_1
     * @param null $rule_2
     * @param null $rule_3
     * @param null $rule_4
     * @param null $rule_5
     * @param null $n_rule_1
     * @param null $n_rule_2
     * @return string
     */
    public static function generate(string $format, string $rule_1, $rule_2 = null, $rule_3 = null, $rule_4 = null, $rule_5 = null, $n_rule_1 = null, $n_rule_2 = null) : string
    {
        $runner = RunnerModel::query()
            ->where('rule_1', '=', $rule_1)
            ->where('rule_2', '=', $rule_2)
            ->where('rule_3', '=', $rule_3)
            ->where('rule_4', '=', $rule_4)
            ->where('rule_5', '=', $rule_5);


        if (!$runner->exists()) {
            $runnerCreate = new RunnerModel();
            $runnerCreate->create([
                'rule_1' => $rule_1,
                'rule_2' => $rule_2,
                'rule_3' => $rule_3,
                'rule_4' => $rule_4,
                'rule_5' => $rule_5,
                'runner' => 1
            ]);

            return self::replaceByFormat($format, 1, $rule_1, $rule_2, $rule_3, $rule_4, $rule_5, $n_rule_1, $n_rule_2);
        }

        $runner = $runner->lockForUpdate()->first();
        $runner->runner = $runner->runner + 1;
        $runner->save();

        return self::replaceByFormat($format, $runner->runner, $rule_1, $rule_2, $rule_3, $rule_4, $rule_5, $n_rule_1, $n_rule_2);
    }

    /**
     * Replace data by format that been provided
     * @param $running_number
     * @param $raw
     * @param $rule_1
     * @param $rule_2
     * @param $rule_3
     * @param $rule_4
     * @param $rule_5
     * @param $n_rule_1
     * @param $n_rule_2
     * @return string
     */
    private static function replaceByFormat($running_number, $raw, $rule_1, $rule_2, $rule_3, $rule_4, $rule_5, $n_rule_1, $n_rule_2): string
    {
        $running_number = self::replaceByRule($running_number, $rule_1, $rule_2, $rule_3, $rule_4, $rule_5);
        $running_number = self::replaceByNRule($running_number, $n_rule_1, $n_rule_2);
        $running_number = self::replaceByRaw($running_number, $raw);
        $running_number = self::replaceByPRaw($running_number, $raw);

        return $running_number;
    }

    /**
     * Replace data by rule_*
     * @param $running_number
     * @param $rule_1
     * @param $rule_2
     * @param $rule_3
     * @param $rule_4
     * @param $rule_5
     * @return string
     */
    private static function replaceByRule($running_number, $rule_1, $rule_2, $rule_3, $rule_4, $rule_5): string
    {
        for ($i = 1; $i <= 5; $i++) {
            if(${'rule_' . $i} != null) {
                $running_number = str_replace('{rule_' . $i . '}', ${'rule_' . $i}, $running_number);
            }
        }

        return $running_number;
    }

    /**
     * Replace data by n_rule_*
     * @param $running_number
     * @param $n_rule_1
     * @param $n_rule_2
     * @return string
     */
    private static function replaceByNRule($running_number, $n_rule_1, $n_rule_2): string
    {
        for ($i = 1; $i <= 2; $i++) {
            if(${'n_rule_' . $i} != null) {
                $running_number = str_replace('{n_rule_' . $i . '}', ${'n_rule_' . $i}, $running_number);
            }
        }

        return $running_number;
    }

    /**
     * Replace data by raw
     * @param $running_number
     * @param $raw
     * @return string
     */
    private static function replaceByRaw($running_number, $raw): string
    {
        return str_replace('{raw}', $raw, $running_number);
    }

    /**
     * Replace data by p_raw
     * @param $running_number
     * @param $raw
     * @return string
     */
    private static function replaceByPRaw($running_number, $raw): string
    {
        $padding = [];

        preg_match('/{p_raw,(\d+)}/', $running_number, $padding);

        if(count($padding) > 0) {
            $running_number = preg_replace('%{p_raw,(\d+)}%', str_pad($raw, $padding[1], '0', STR_PAD_LEFT), $running_number);
        }

        return $running_number;
    }
}
