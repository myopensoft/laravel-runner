<?php

namespace MyOpensoft\Runner;

use Illuminate\Support\ServiceProvider;

class RunnerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // something here
    }

    /**
     * Bootstrap services.s
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/migrations/2018_07_02_014800_create_runners_table.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations/2023_12_15_014800_add_composit_index_to_runners.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations/2024_06_12_014800_make_rule_nullable_to_runners.php');
    }
}
