<?php

namespace MyOpensoft\Runner;

use Illuminate\Database\Eloquent\Model as Model;

class RunnerModel extends Model
{
    public $table = 'runners';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'rule_1',
        'rule_2',
        'rule_3',
        'rule_4',
        'rule_5',
        'runner'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rule_1' => 'string',
        'rule_2' => 'string',
        'rule_3' => 'string',
        'rule_4' => 'string',
        'rule_5' => 'string',
        'runner' => 'integer',
    ];
}

