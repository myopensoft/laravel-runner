1.1.7 - Update to handle race condition issue when run with DB::transaction().

1.0.6 - Update to PHP 8.1,8.2 laravel 10.

1.0.5 - Change migration for runner column from string to integer due to pgqsl cannot use increment() if column is varchar.

1.0.1 - Change public function to private & add to packagist.

1.0.0 - Initial commit
